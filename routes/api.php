<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::group(['prefix' => 'v1/document-service', 'middleware' => ['jwt']], function(){
        Route::get('/','DocumentController@rootList');
        Route::post('/document','DocumentController@setDocument');
        Route::get('/document/{document_id}','DocumentController@detailDocument');
        Route::delete('/document','DocumentController@deleteDocument');

        Route::post('/folder','FolderController@setFolder');
        Route::get('/folder/{folder_id}','DocumentController@listFilePerFolder');
        Route::delete('/folder','FolderController@deleteFolder');
    });

