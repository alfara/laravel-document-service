<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Folder
 * @package App
 * @mixin Eloquent
 */

class Folder extends Model
{
    use SoftDeletes;

    protected $table = 'folders';
    protected $keyType = 'string';

    protected $fillable = [
        'name','type','is_public','owner_id','share','id','timestamp'
    ];

    protected $casts = [
      'share' => 'array',
        'is_public' => 'boolean'
    ];

    protected $hidden = [
        'created_at','deleted_at','updated_at'
    ];

    public $incrementing = false;
}
