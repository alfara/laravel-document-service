<?php

use Illuminate\Database\Seeder;

class FolderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $folders = array(
            array('id' => '82b07a6f-60cc-4403-8fd2-329ef0de0d3e','name' => 'Folder Baru','type' => 'folder','is_public' => '1','owner_id' => '150','share' => NULL,'timestamp' => '16576232323','created_at' => '2021-02-18 09:53:33','updated_at' => '2021-02-18 09:59:00','deleted_at' => NULL),
            array('id' => '82b07a6f-60cc-4403-8fd2-329ef0dy0d3d','name' => 'Folder Baru','type' => 'folder','is_public' => '1','owner_id' => '150','share' => NULL,'timestamp' => '16576232323','created_at' => '2021-02-18 09:50:02','updated_at' => '2021-02-18 09:50:02','deleted_at' => NULL)
        );

        \App\Folder::insert($folders);
    }
}
