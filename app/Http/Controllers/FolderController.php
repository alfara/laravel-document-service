<?php

namespace App\Http\Controllers;

use App\Folder;
use Illuminate\Http\Request;
use DB;
use Str;
use JWTAuth;
use Throwable;

class FolderController extends Controller
{

    protected $payload;

    public function __construct()
    {
        $this->middleware('jwt');
        $this->payload = getPayload();
    }

    public function setFolder(Request $request){
        $input = $request->all();

        $input['owner_id'] = $this->payload['owner_id'];
        $input['company_id'] = $this->payload['company_id'];

        $folder = Folder::where('id',$input['id'])->first();
        try {
            DB::beginTransaction();
            if ($folder){
                $folder->update($input);
                DB::commit();
                return response()->json([
                    'error' => false,
                    'message' => 'id exist, update the folder',
                    'data' => $folder
                ],200);
            }else{
                $folder = Folder::create($input);
                DB::commit();
                return response()->json([
                    'error' => false,
                    'message' => 'folder created',
                    'data' => $folder
                ],200);
            }
        }catch (Throwable $exception){
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ],400);
        }
    }

    public function deleteFolder(Request $request){
        $id = $request->get('id');
        $folder = Folder::where('id',$id)->first();
        if (!$folder){
            return response()->json([
                'error' => true,
                'message' => 'folder not found'
            ],400);
        }
        try {
            DB::beginTransaction();
            $folder->delete();
            DB::commit();
            return response()->json([
               'error' => false,
               'message' => 'success delete folder'
            ],200);
        }catch (Throwable $exception){
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ],400);
        }
    }
}
