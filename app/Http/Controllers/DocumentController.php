<?php

namespace App\Http\Controllers;

use App\Document;
use App\Folder;
use Illuminate\Http\Request;
use JWTAuth;
use DB;
use Throwable;

class DocumentController extends Controller
{
    protected $payload;

    public function __construct()
    {
        $this->middleware('jwt');
        $this->payload = getPayload();
    }

    public function rootList(){
        try {
            $datas = [];
            $documents = Document::all();
            if ($documents){
                foreach ($documents as $key => $document){
                    $datas[$key] = [
                        'id' => $document->id,
                        'name' => $document->name,
                        'type' => $document->type,
                        'is_public' => $document->folder->is_public ?? null,
                        'owner_id' => $document->owner_id,
                        'share' => $document->share,
                        'timestamp' => $document->timestamp,
                        'company_id' => $this->payload['company_id']
                    ];
                }
            }
            return response()->json([
                'error' => false,
                'data' => $datas
            ],200);
        }catch (Throwable $exception){
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ],400);
        }
    }

    public function listFilePerFolder($folder_id)
    {
        try {
            $documents = Document::where('folder_id', $folder_id)->get();
            return response()->json([
                'error' => false,
                'data' => $documents
            ], 200);
        } catch (Throwable $exception) {
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ], 400);
        }
    }

    public function setDocument(Request $request){
        $input = $request->all();

        $document = Document::where('id',$input['id'])->first();
        try {
            DB::beginTransaction();
            if ($document){
                $document->update($input);
                DB::commit();
                return response()->json([
                    'error' => false,
                    'message' => 'id exist, update the document',
                    'data' => $document
                ],200);
            }else{
                $document = document::create($input);
                DB::commit();
                return response()->json([
                    'error' => false,
                    'message' => 'document created',
                    'data' => $document
                ],200);
            }
        }catch (Throwable $exception){
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ],400);
        }
    }

    public function detailDocument($document_id){
        try {
            $document = Document::where('id',$document_id)->first();
            return response()->json([
                'error' => false,
                'message' => 'success get document',
                'data' => $document
            ],200);
        }catch (Throwable $exception){
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ],400);
        }
    }

    public function deleteDocument(Request $request){
        $id = $request->get('id');
        $document = Document::where('id',$id)->first();
        if (!$document){
            return response()->json([
                'error' => true,
                'message' => 'document not found'
            ],400);
        }
        try {
            DB::beginTransaction();
            $document->delete();
            DB::commit();
            return response()->json([
                'error' => false,
                'message' => 'success delete document'
            ],200);
        }catch (Throwable $exception){
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ],400);
        }
    }

}
