<?php

use Illuminate\Database\Seeder;

class DocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $documents = array(
            array('id' => '82b07a6f-60cc-4403-8fd2-329ef0de045d','name' => 'Document Job desc Tech update','type' => 'document','folder_id' => '82b07a6f-60cc-4403-8fd2-329ef0de0d3e','content' => '{"blocks":[{"type":"paragraph","text":"This is paragraph"}]}','owner_id' => '123','share' => '[1,23,4232,121]','company_id' => '23','timestamp' => '1605081795','created_at' => '2021-02-18 10:21:41','updated_at' => '2021-02-18 11:07:37','deleted_at' => NULL),
            array('id' => '82b07a6f-60cc-4403-8fd2-329ef0dr045d','name' => 'Document Job desc Tech update','type' => 'document','folder_id' => '82b07a6f-60cc-4403-8fd2-329ef0dy0d3d','content' => '{"blocks":[{"type":"paragraph","text":"This is paragraph"}]}','owner_id' => '123','share' => '[1,23,4232,121]','company_id' => '23','timestamp' => '1605081795','created_at' => '2021-02-18 11:13:03','updated_at' => '2021-02-18 11:13:03','deleted_at' => NULL)
        );

        \App\Document::insert($documents);
    }
}
