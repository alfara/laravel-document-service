### Setup
1. clone this project
2. run <code>composer install</code>
3. setup your database
4. setup your .env
5. run <code>php artisan migrate:refresh --seed</code> to dump data that I make
6. run <code>php artisan config:cache</code>
7. run <code>php artisan config:clear</code>
8. run <code>php artisan serve</code>

### Docs
[Click here to see my postman documentation](https://documenter.getpostman.com/view/9643281/TWDWHwoZ) <br>
[Click here to see my socmed](https://www.linkedin.com/in/alfarand/)
