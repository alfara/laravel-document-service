<?php
function getPayload(){
    $token = JWTAuth::getToken();
    if ($token){
        return JWTAuth::getPayload($token)->toArray();
    }else{
        return response()->json('error',400);
    }
}
