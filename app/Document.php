<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Document
 * @package App
 * @mixin Eloquent
 */

class Document extends Model
{
    use SoftDeletes;

    protected $table = 'documents';
    protected $keyType = 'string';

    protected $fillable = [
        'name','type','folder_id','content','owner_id','share','company_id','id','timestamp'
    ];

    protected $casts = [
      'content' =>'array',
        'share' => 'array',
    ];

    protected $hidden = [
        'created_at','deleted_at','updated_at'
    ];

    public $incrementing = false;

    public function folder(){
        return $this->hasOne(Folder::class,'id','folder_id');
    }
}
